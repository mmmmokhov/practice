import React from "react";
import {Admin, Resource} from 'react-admin';
import restProvider from "ra-data-simple-rest";
import ProductList from './Products/ProductList';
import ProductCreate from './Products/ProductCreate';
import ProductEdit from './Products/ProductEdit';
import CustomerList from './Customers/CustomerList';
import CustomerCreate from './Customers/CustomerCreate';
import CustomerEdit from './Customers/CustomerEdit';
import CartList from './Cart/CartList';
import CartCreate from './Cart/CartCreate';
import CartEdit from './Cart/CartEdit';

function App() {
  return <Admin dataProvider={restProvider('http://localhost:3000')}>
    <Resource name='products' list={ProductList} create={ProductCreate} edit={ProductEdit}/>
    <Resource name='customers' list={CustomerList} create={CustomerCreate} edit={CustomerEdit}/>
    <Resource name='cart' list={CartList} create={CartCreate} edit={CartEdit}/>
  </Admin>
}

export default App;
