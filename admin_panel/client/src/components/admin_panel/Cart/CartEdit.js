import React from "react";
import { Edit, SimpleForm, TextInput, DateInput } from "react-admin";

const CartEdit = (props) => {
  return (
    <Edit title="Edit Customer" {...props}>
      <SimpleForm>
        <TextInput source="id" />
        <TextInput source="customerId" />
        <TextInput source="products" />
        <DateInput source="date" />
      </SimpleForm>
    </Edit>
  );
};

export default CartEdit;
