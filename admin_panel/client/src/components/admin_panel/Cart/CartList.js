import React from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  DeleteButton,
  EmailField,
} from "react-admin";

const CartList = (props) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="customerId" />
        <TextField source="products" />
        <DateField source="date" />
        <EditButton basePath="/cart" />
        <DeleteButton basePath="/cart" />
      </Datagrid>
    </List>
  );
};

export default CartList;
