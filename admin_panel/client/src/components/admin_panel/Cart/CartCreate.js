import React from "react";
import {
  Create,
  SimpleForm,
  TextInput,
  DateInput
} from "react-admin";

const CartCreate = (props) => {
  return (
    <Create title="Create a Customer" {...props}>
      <SimpleForm>
        <TextInput source="id" />
        <TextInput source="customerId" />
        <TextInput source="products" />
        <DateInput source="date" />
      </SimpleForm>
    </Create>
  );
};

export default CartCreate;
