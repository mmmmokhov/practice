import React from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  DeleteButton,
} from "react-admin";

const ProductList = (props) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="itemNo" />
        <TextField source="enabled" />
        <TextField source="name" />
        <TextField source="currentPrice" />
        <TextField source="previousPrice" />
        <TextField source="categories" />
        <TextField source="imageUrls" />
        <TextField source="quantity" />
        <TextField source="color" />
        <TextField source="sizes" />
        <TextField source="productUrl" />
        <TextField source="brand" />
        <TextField source="manufacturer" />
        <TextField source="manufacturerCountry" />
        <TextField source="seller" />
        <DateField source="date" />
        <TextField source="description" />
        <EditButton basePath="/products" />
        <DeleteButton basePath="/products" />
      </Datagrid>
    </List>
  );
};

export default ProductList;
