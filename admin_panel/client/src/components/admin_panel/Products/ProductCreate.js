import React from "react";
import { Create, SimpleForm, TextInput, DateInput } from "react-admin";

const ProductCreate = (props) => {
  return (
    <Create title="Create a Product" {...props}>
      <SimpleForm>
        <TextInput source="id" />
        <TextInput source="itemNo" />
        <TextInput source="enabled" />
        <TextInput source="name" />
        <TextInput source="currentPrice" />
        <TextInput source="previousPrice" />
        <TextInput source="categories" />
        <TextInput source="imageUrls" />
        <TextInput source="quantity" />
        <TextInput source="color" />
        <TextInput source="sizes" />
        <TextInput source="productUrl" />
        <TextInput source="brand" />
        <TextInput source="manufacturer" />
        <TextInput source="manufacturerCountry" />
        <TextInput source="seller" />
        <DateInput source="date" />
        <TextInput source="description" />
      </SimpleForm>
    </Create>
  );
};

export default ProductCreate;
