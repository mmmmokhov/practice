import React from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  DeleteButton,
  EmailField,
} from "react-admin";

const CustomerList = (props) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="isAdmin" />
        <TextField source="enabled" />
        <TextField source="firstName" />
        <TextField source="lastName" />
        <TextField source="login" />
        <EmailField source="email" />
        <TextField source="password" />
        <TextField source="telephone" />
        <TextField source="gender" />
        <TextField source="avatarUrl" />
        <TextField source="customerNo" />
        <DateField source="date" />
        <EditButton basePath="/customers" />
        <DeleteButton basePath="/customers" />
      </Datagrid>
    </List>
  );
};

export default CustomerList;
