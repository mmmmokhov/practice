import React from "react";
import {
  Create,
  SimpleForm,
  TextInput,
  DateInput
} from "react-admin";

const CustomerCreate = (props) => {
  return (
    <Create title="Create a Customer" {...props}>
      <SimpleForm>
        <TextInput source="id" />
        <TextInput source="isAdmin" />
        <TextInput source="enabled" />
        <TextInput source="firstName" />
        <TextInput source="lastName" />
        <TextInput source="login" />
        <TextInput source="email" />
        <TextInput source="password" />
        <TextInput source="telephone" />
        <TextInput source="gender" />
        <TextInput source="avatarUrl" />
        <TextInput source="customerNo" />
        <DateInput source="date" />
      </SimpleForm>
    </Create>
  );
};

export default CustomerCreate;
