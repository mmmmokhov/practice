import React from "react";
import { Edit, SimpleForm, TextInput, DateInput } from "react-admin";

const CustomerEdit = (props) => {
  return (
    <Edit title="Edit Customer" {...props}>
      <SimpleForm>
        <TextInput source="id" />
        <TextInput source="isAdmin" />
        <TextInput source="enabled" />
        <TextInput source="firstName" />
        <TextInput source="lastName" />
        <TextInput source="login" />
        <TextInput source="email" />
        <TextInput source="password" />
        <TextInput source="telephone" />
        <TextInput source="gender" />
        <TextInput source="avatarUrl" />
        <TextInput source="customerNo" />
        <DateInput source="date" />
      </SimpleForm>
    </Edit>
  );
};

export default CustomerEdit;
